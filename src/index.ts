const pointsElement = document.getElementById('points') as HTMLHeadElement;
const canvas = document.getElementById('game') as HTMLCanvasElement;
const ctx = canvas?.getContext('2d');

const width: number = 10;
const height: number = 10;


interface Point {
    x: number;
    y: number;
}

let snakeBody: Point[] = [];
let snakeHead: Point = {
    x: 10,
    y: 10
};
let fruit: Point = {
    x: getRandomPosition(canvas.width),
    y: getRandomPosition(canvas.height)
}
let points = 0;

document.addEventListener('keypress', press);
let key: string;


function press(e: any) {
    key = e.key;
}

function clearCanvas() {
    ctx?.clearRect(0, 0, canvas.width, canvas.height);
}

function resolveMovement(key: string) {
    switch (key) {
        case "w":
            snakeHead.y -= 10;
            break;
        case "s":
            snakeHead.y += 10;
            break;
        case "a":
            snakeHead.x -= 10;
            break;
        case "d":
            snakeHead.x += 10;
            break
    }
}

function resolveBoundaries() {
    if (snakeHead.x === canvas.width) {
        snakeHead.x = -10;
        return
    }

    if (snakeHead.x === -10) {
        snakeHead.x = canvas.width;
        return;
    }

    if (snakeHead.y === canvas.height) {
        snakeHead.y = -10;
        return;
    }

    if (snakeHead.y === -10) {
        snakeHead.y = canvas.height;
    }
}

function paint() {

    ctx!.fillStyle = "maroon";
    ctx?.fillRect(fruit.x, fruit.y, width, height);

    console.log("hello")
    ctx!.fillStyle = "blue";
    for (let i = 0; i < snakeBody.length; i++) {
        const bodyPart = snakeBody.at(i);
        ctx?.fillRect(bodyPart!.x, bodyPart!.y, width, height)
    }

    ctx!.fillStyle = "black";
    ctx?.fillRect(snakeHead.x, snakeHead.y, width, height);

}

function getRandomPosition(max: number): number {
    const value = Math.floor(Math.random() * (max - 10));
    return Math.ceil(value / 10) * 10;
}

function addPoint() {
    points++;
    pointsElement.innerHTML = `Points: ${points}`;
}

function resolveFruitCrash() {
    if (snakeHead.x === fruit.x && snakeHead.y === fruit.y) {
        fruit.x = getRandomPosition(canvas.width);
        fruit.y = getRandomPosition(canvas.height);
        snakeBody.unshift({x: snakeHead.x, y: snakeHead.y})
        addPoint();
    }
}

function snakeMovement() {
    if (snakeBody.length === 0) {
        return;
    }

    for (let i = snakeBody.length - 1; i > 0; i--) {
        const tmpBodyPart = snakeBody.at(i - 1);
        snakeBody[i] = {x: tmpBodyPart!.x, y: tmpBodyPart!.y};
    }

    snakeBody[0] = {x: snakeHead.x, y: snakeHead.y}
}

setInterval(() => {
    clearCanvas()
    if (key)
        resolveMovement(key);
    paint();

    //TODO Resolve crash with body
    //TODO Resolve back-movement
    resolveFruitCrash();
    resolveBoundaries();
    snakeMovement();
}, 100);

export {};